import 'dart:convert';
import 'dart:typed_data';
import 'package:argon2/argon2.dart';
import 'package:encrypt/encrypt.dart' as ecrypt;
import 'package:crypton/crypton.dart' as crypton;

String Argon2(String password, bool generateKey) {
  var salt = generateKey
      ? 'WhyW0uldAS@n3P3rs0nUs3AW3@kP@ssw0rd@nyway?'.toBytesLatin1()
      : 'Th1s!s4n3xtr3m3lyl0ngs@ltt0av01dra!nb0ws'.toBytesLatin1();
  var parameters = Argon2Parameters(
    Argon2Parameters.ARGON2_i,
    salt,
    version: Argon2Parameters.ARGON2_VERSION_10,
    iterations: 2,
    memoryPowerOf2: 16,
  );
  var argon2 = Argon2BytesGenerator();
  argon2.init(parameters);
  var passwordBytes = parameters.converter.convert(password);
  var result = Uint8List(32);
  argon2.generateBytes(passwordBytes, result, 0, result.length);
  var resultHex = result.toHexString();
  return resultHex;
}

String keyAES(stringText, givenKey, encrypt) {
  final key = ecrypt.Key.fromBase16(givenKey);
  final encrypter = ecrypt.Encrypter(ecrypt.AES(key));
  final ecrypt.IV iv = ecrypt.IV.fromBase16('deadbeef');
  late final cipher;
  if (encrypt) {
    final encrypted = encrypter.encrypt(stringText, iv: iv);
    cipher = encrypted.base64;
  } else {
    final decrypted =
        encrypter.decrypt(ecrypt.Encrypted.fromBase64(stringText), iv: iv);
    cipher = decrypted;
  }
  return cipher;
}

List<String> messageAES(stringText, asyKey, encrypt, randomKey, isPriv) {
  final String asymmetricKey = asyKey;
  final ecrypt.IV iv = ecrypt.IV.fromBase16('deadbeef');
  late final List<String> cipher;
  if (encrypt) {
    final key = ecrypt.Key.fromBase16(
        randomKey); // turn the hex random text key into a key
    final msgEncrypter =
        ecrypt.Encrypter(ecrypt.AES(key)); // use it to encrypt the message text
    final encrypted = msgEncrypter.encrypt(stringText, iv: iv);

    final interPublicKey = crypton.RSAPublicKey.fromPEM(asymmetricKey);
    // final publicKey =
    //     crypton.RSAPublicKey(interPublicKey.modulus!, interPublicKey.exponent!);
    cipher = [
      //encrypt the random key with the public key,

      interPublicKey.encrypt(randomKey),
      encrypted.base64
    ]; // return the encrypted random key, text encrypted with the random key
  } else {
    // turn the RSA string into RSA private key

    late final parsedAsyKey;
    late final ecrypt.Key decRandomKey;
    if (isPriv) {
      parsedAsyKey = crypton.RSAPrivateKey.fromPEM(asymmetricKey);
      //     parsedAsyKey.modulus!,
      //     parsedAsyKey.exponent!,
      //     parsedAsyKey.p,
      //     parsedAsyKey.q);
      decRandomKey = ecrypt.Key.fromBase16(
          parsedAsyKey.decryptData(utf8.encode(randomKey)));
    } else {
      cipher = ['only the recipient can view the unencrypted message'];
      return cipher;
    }

    final msgEncrypter = ecrypt.Encrypter(ecrypt.AES(decRandomKey));
    final decrypted =
        msgEncrypter.decrypt(ecrypt.Encrypted.fromBase64(stringText), iv: iv);
    cipher = [decrypted];
  }
  return cipher;
}
